package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"
)

var helloList = []string{
	"Hello, world",
	"Сәлем Әлем",
	"Witaj świecie",
	"Hola Mundo",
	"Привет, мир",
}

func main() {
	rand.Seed(time.Now().UnixNano())
	index := rand.Intn(len(helloList))
	hello_msg, err := hello(index)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(hello_msg)
}

func hello(index int) (string, error) {
	return helloList[index], nil
}
