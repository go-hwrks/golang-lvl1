/*
1. Напишите программу для вычисления площади прямоугольника. Длины сторон прямоугольника должны вводиться пользователем с клавиатуры.
2. Напишите программу, вычисляющую диаметр и длину окружности по заданной площади круга. Площадь круга должна вводиться пользователем с клавиатуры.
3. С клавиатуры вводится трехзначное число. Выведите цифры, соответствующие количество сотен, десятков и единиц в этом числе.
*/
package main

import (
	"fmt"
	"log"
	"math"
	"os"
	"os/exec"
	"strconv"
)

func main() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
	b, _ := chooseFunc()
	if b == 4 {
		fmt.Println("Выход из программы")
	} else if b == 1 {
		a, err := rectangleArea()
		if err != nil {
			log.Fatal("Случилась ошибка", err)
		}
		fmt.Println("**************************\nРезультат:", a)
	} else if b == 2 {
		a, b, err := circumferenceAndDiameter()
		if err != nil {
			log.Fatal("Случилась ошибка", err)
		}
		fmt.Printf("**************************\nРезультат:\nДиаметр:%26.2f\nДлина окружности: %16.2f\n", a, b)
	} else if b == 3 {
		a, b, c, err := getNumbers()
		if err != nil {
			log.Fatal("Случилась ошибка", err)
		}
		fmt.Println("**************************\nРезультат:\nПервая цифра: " + strconv.Itoa(int(a)) + "\nВторая цифра: " + strconv.Itoa(int(b)) + "\nТретья цифра: " + strconv.Itoa(int(c)))
	} else {
		fmt.Println("Вы ввели не то, введите заново")
	}
}

func chooseFunc() (a int, err error) {
	multiline := "**************************\n" +
		"Выберите действие\n" +
		"1 - Вычислить площадь прямоугольника \n" +
		"2 - Вычислить диаметр и длину окружности \n" +
		"3 - Получить цифры в составе трёхзначного числа \n" +
		"4 - Выйти из программы \n" +
		"* Введите цифру от 1 до 4: "
	fmt.Println(multiline)
	_, err = fmt.Scan(&a)
	return a, err
}

func rectangleArea() (res int, err error) {
	fmt.Println("Введите два числа: ")
	var length, height int
	_, err = fmt.Scan(&length, &height)
	res = length * height
	return res, err
}

func circumferenceAndDiameter() (diameter float64, length float64, err error) {
	var circleSquare uint
	fmt.Println("Введите площадь круга: ")
	_, err = fmt.Scan(&circleSquare)
	diameter = math.Sqrt(float64(circleSquare)/math.Pi) * 2
	length = math.Pi * diameter
	return diameter, length, err
}

func getNumbers() (a uint, b uint, c uint, err error) {
	fmt.Println("Введите трехзначное число: ")
	var number uint
	_, err = fmt.Scan(&number)
	if number < 1000 && number > 99 {
		a = number / 100
		b = number / 10 % 10
		c = number % 10
	} else {
		fmt.Println("Вы что-то не то ввели: ")
	}
	return a, b, c, err
}
