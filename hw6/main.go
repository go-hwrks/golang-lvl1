package main

import (
	"fmt"

	"gitlab.com/go-hwrks/golang-lvl1/hw6/pointers"
)

func main() {
	fmt.Println("Работа над ошибками и немного экспериментов с указателями")

	var myFloatPointer *float64 = pointers.CreatePointer()
	fmt.Println("Печать значения по указателю:", *myFloatPointer)
	// Печатаю значение на которое указывает указатель

	myBool := true
	pointers.PrintPointer(&myBool)

	pointers.PrintTypeMyBool()
	pointers.PrintTypeMyInt()
	pointers.PrintTypeMyString()
	pointers.PrintTypeMyTime()

	amount := 6
	pointers.Double(&amount)
	fmt.Println("Использование указателя вместо переменной и умножение на два:", amount)

	a, b := 5, 10
	pointers.Swap(&a, &b)
	fmt.Println("Замена переменных при помощи указателей:", "a ==", a, a == 10, "b ==", b, b == 5)

	//пробую работать со структурами и указателями
	d := pointers.Person{"Дмитрий", 10}
	pointers.ShowMePerson(d)
	fmt.Println("======================")
	pointers.ShowMePerson2(&d)

	//Слайсы и указатели
	var parrentArray = [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	mySlice := parrentArray[2:5]
	fmt.Println("Родительский массив", parrentArray)
	fmt.Println("Срез, указывающий на parrentArray[2:5]", mySlice)
	parrentArray[2] = 15
	fmt.Println("Изменил 3-й элемент в рдительском массиве", parrentArray)
	fmt.Println("В срезе элемент тоже поменялся", mySlice)

	//Передаю в функцию указатель на массив
	fmt.Printf("сложение массива по указателю: %d\n", pointers.SumPointerToArray(&parrentArray))
	fmt.Printf("сложение массива как среза: %d\n", pointers.SumSlice(mySlice))
}
