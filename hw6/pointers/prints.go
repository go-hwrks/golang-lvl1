package pointers

import (
	"fmt"
	"reflect"
	"time"
)

func CreatePointer() *float64 {
	myFloat := 98.5

	return &myFloat
}

func PrintTypeMyFloat() {
	var myFloat float64
	fmt.Println("Печатаем тип &myFloat", reflect.TypeOf(&myFloat))
}

func PrintTypeMyBool() {
	var myBool bool
	fmt.Println("Печатаем тип &myBool", reflect.TypeOf(&myBool))
}

func PrintTypeMyInt() {
	var myInt int
	fmt.Println("Печатаем тип &myInt", reflect.TypeOf(&myInt))
}

func PrintTypeMyString() {
	var myString string
	fmt.Println("Печатаем тип &myString", reflect.TypeOf(&myString))
}

func PrintTypeMyTime() {
	myTime := &time.Time{}
	if myTime != nil {
		fmt.Printf("Печатаем указателя на время: %#v\n", *myTime)
		fmt.Printf("Печать времени: %#v\n", myTime.String())
	}
}

func PrintPointer(myBoolPointer *bool) {
	fmt.Println("Печатаем значение по указателю", *myBoolPointer)
}

func Double(number *int) {
	*number *= 2
}

func Swap(a *int, b *int) {
	*a, *b = *b, *a
}
