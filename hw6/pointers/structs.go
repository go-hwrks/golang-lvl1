package pointers

import (
	"fmt"
)

type Person struct {
	Name string
	Age  int
}

func ShowMePerson(d Person) {
	fmt.Printf("Передаём в функцию структуру целиком. \nИмя: %s\nВозраст: %d\n", d.Name, d.Age)
}

func ShowMePerson2(d *Person) {
	fmt.Printf("Передаём в функцию указатель на структуру. \nИмя: %s\nВозраст: %d\n", d.Name, d.Age)
}
