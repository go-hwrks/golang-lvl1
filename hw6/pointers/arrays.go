package pointers

func SumPointerToArray(a *[10]int) (sum int) {
	for _, value := range *a {
		sum += value
	}
	return
}
func SumSlice(a []int) (sum int) {
	for _, value := range a {
		sum += value
	}
	return
}
