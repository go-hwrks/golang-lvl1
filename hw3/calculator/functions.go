package calculator

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
)

func Add(a *float64, b *float64, res *float64) {
	fmt.Print("введите первое слагаемое ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите второе слагаемое ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = *a + *b
}

func Subt(a *float64, b *float64, res *float64) {
	fmt.Print("введите первое число ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите второе число ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = *a - *b
}

func Mult(a *float64, b *float64, res *float64) {
	fmt.Print("введите первый множитель ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите второй множитель ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = *a * *b
}

func Div(a *float64, b *float64, res *float64) {
	fmt.Print("введите делимое ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите делитель ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = *a / *b
}

func Perc(a *float64, b *float64, res *float64) {
	fmt.Print("введите число ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите процент который вы хотите получить ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = *a / 100 * *b
}

func Sqroot(a *float64, b *float64, res *float64) {
	fmt.Print("введите число ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите степень корня ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = math.Pow(*a, 1.0 / *b)
}

func Pow(a *float64, b *float64, res *float64) {
	fmt.Print("введите число ")
	_, err1 := fmt.Scanln(a)
	if err1 != nil {
		log.Fatal("Вы ввели что-то не то", err1)
	}
	fmt.Print("введите степень ")
	_, err2 := fmt.Scanln(b)
	if err2 != nil {
		log.Fatal("Вы ввели что-то не то", err2)
	}
	*res = math.Pow(*a, *b)
}

func SimpleNum() {
	fmt.Print("введите число чтобы узнать сколько простых чисел от 1 до него ")
	var a int
	k := 0
	var lst = []int{}
	_, err := fmt.Scanln(&a)
	if err != nil {
		log.Fatal("Вы ввели что-то не то", err)
	}
	for i := 1; i <= a; i++ {
		for j := 1; j <= i; j++ {
			if i%j == 0 && i > 2 {
				k += 1
			}
		}
		if k <= 2 {
			lst = append(lst, i)

		} else {
			k = 0
		}
	}
	fmt.Println("Простые числа: ", lst)
}

func InSortFunc() {

	fmt.Println("Введите числа через пробел, чтобы отсортировать их: ")
	//секция объявления и приёма данных
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	ar := strings.Fields(text)
	var ar1 []int
	for _, item := range ar {
		a, err := strconv.Atoi(strings.Trim(item, "\r\n"))
		if err == nil {
			ar1 = append(ar1, a)
		}
	}
	for i := 0; i < len(ar1); i++ {
		x := ar1[i]
		j := i
		for ; j >= 1 && ar1[j-1] > x; j-- {
			ar1[j] = ar1[j-1]
		}
		ar1[j] = x
	}
	//вывод результата
	fmt.Printf("%v", ar1)
}
