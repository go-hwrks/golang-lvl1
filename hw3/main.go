package main

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/go-hwrks/golang-lvl1/hw3/calculator"
	"gitlab.com/go-hwrks/golang-lvl1/hw3/menu"
)

const (
	addition menu.Menu = iota
	subtract
	multiply
	divide
	percent
	squareRoot
	power
	simple
	inSort
)

func main() {
	var a, b, res float64

	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()

	act := menuFunc()

	switch act {
	case 1:
		calculator.Add(&a, &b, &res)
	case 2:
		calculator.Subt(&a, &b, &res)
	case 3:
		calculator.Mult(&a, &b, &res)
	case 4:
		calculator.Div(&a, &b, &res)
	case 5:
		calculator.Perc(&a, &b, &res)
	case 6:
		calculator.Sqroot(&a, &b, &res)
	case 7:
		calculator.Pow(&a, &b, &res)
	case 8:
		calculator.SimpleNum()
		os.Exit(0)
	case 9:
		calculator.InSortFunc()
		os.Exit(0)
	default:
		fmt.Println("Операция выбрана неверно")
		os.Exit(0)
	}

	fmt.Printf("Результат выполнения операции: %.2f\n", res)
}
func menuFunc() int {
	actions := [...]string{addition.String(), subtract.String(), multiply.String(), divide.String(), percent.String(), squareRoot.String(), power.String(), simple.String(), inSort.String()}
	fmt.Println("Введите арифметическую операцию: ")
	for i, j := range actions {
		fmt.Printf("%d - %s\n", i+1, j)
	}

	var act int
	fmt.Scanln(&act)
	return act
}
