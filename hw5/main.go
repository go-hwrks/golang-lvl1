package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
)

const (
	getFibNum Menu = iota
	getFibMap
	exitLoop
)

func main() {
	fibonacci := make(map[int]int)
	var act int
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	for act != 2 {
		c.Run()
		act = menu()
		switch act {
		case 0:
			var n int
			fmt.Print("Введите число: ")
			_, err := fmt.Scanln(&n)
			if err != nil {
				log.Fatal("Вы сделали ошибку при вводе\n", err)
			}
			//index := strconv.Itoa(n)
			//fibonacci[n] = fib(n)
			userMapAsChache(n, &fibonacci)
			fmt.Println("Базовое число: "+strconv.Itoa(n)+". Число Фибоначчи:", fibonacci[n])
		case 1:
			fmt.Println("Выводим список полученных чисел", fibonacci)
		case 2:
			fmt.Println("Выходим из программы")
			//break forLoop
		default:
			fmt.Println("Вы что-то не то ввели")
		}
	}
}

func userMapAsChache(i int, m *map[int]int) {
	if _, a := (*m)[i]; !a {
		fmt.Println("Числа в кэше нет, вычисляем")
		(*m)[i] = fib(i)
	}
}

func fib(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	} else {
		return fib(n-1) + fib(n-2)
	}
}

func menu() int {
	arr := [...]string{getFibNum.String(), getFibMap.String(), exitLoop.String()}
	fmt.Println("Введите номер меню:")
	for i, item := range arr {
		fmt.Println(i, "-", item)
	}
	var x int
	fmt.Scanln(&x)
	return x
}
