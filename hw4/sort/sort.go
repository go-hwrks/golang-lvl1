package sort

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

//Оставил себе на память
/*func PermutationSortOld() []int {

	fmt.Println("Введите числа через пробел, чтобы отсортировать их: ")
	//секция объявления и приёма данных
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	ar := strings.Fields(text)
	var ar1 []int

	for _, item := range ar {
		a, err := strconv.Atoi(strings.Trim(item, "\r\n"))
		if err == nil {
			ar1 = append(ar1, a)
		}
	}
	//секция сортировки
	for i := 0; i < len(ar1); i++ {
		x := ar1[i]
		j := i
		for ; j >= 1 && ar1[j-1] > x; j-- {
			ar1[j] = ar1[j-1]
		}
		ar1[j] = x
	}
	//возврат результата
	return ar1
}*/
//функция с указателем
//func PermutationSort(ar *[]int) {
//	//секция сортировки
//	for i := 0; i < len(*ar); i++ {
//		x := (*ar)[i]
//		for j := i; j >= 1 && (*ar)[j-1] >= x; j-- {
//			(*ar)[j] = (*ar)[j-1]
//			(*ar)[j-1] = x
//		}
//	}
//}

func PermutationSort(ar []int) []int {
	var newArr = make([]int, len(ar))
	copy(newArr, ar)
	for i := 0; i < len(newArr); i++ {
		x := (newArr)[i]
		for j := i; j >= 1 && (newArr)[j-1] >= x; j-- {
			(newArr)[j] = (newArr)[j-1]
			(newArr)[j-1] = x
		}
	}
	return newArr
}

//секция объявления и приёма данных
func GetArrayWithNumbers() []int {
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	ar := strings.Fields(text)
	var resultArray []int
	for _, item := range ar {
		a, err := strconv.Atoi(strings.Trim(item, "\r\n"))
		if err != nil {
			fmt.Println("Что-то пошло не так")
			log.Fatal(err)
		}
		resultArray = append(resultArray, a)
	}
	return resultArray
}
