package main

import (
	"fmt"

	"gitlab.com/go-hwrks/golang-lvl1/hw4/sort"
)

func main() {
	fmt.Println("Введите числа через пробел, чтобы отсортировать их: ")
	ar := sort.GetArrayWithNumbers()
	//sort.PermutationSort(&(ar))

	newArr := sort.PermutationSort(ar)
	fmt.Printf("Старый массив: %v\n", ar)
	fmt.Printf("Новый массив:  %v", newArr)

}
